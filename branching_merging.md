# Branching and Merging

## 1. Intro...
Do NOT create the branch on the original idgdevelopers repository!
Create the branch either locally or on your staging area in bitbucket.

## 2. Branching
Let's say we are doing new development but need a quick bug fix...

Call the branch "bug_fix_2"
First, stash away current work.
```
git stash push
```

Create the branch locally
```
git branch bug_fix_2
git pull bug_fix_2
git checkout bug_fix_2
```
Create the file "bf2.txt".
Then add, commit ,a nd push to staging area.
```
git add .
git commit -m "BF 2 "
git push https://perenglundus@bitbucket.org/perenglundus/git_sandbox.git
```


## 3. Merging

### 3.1. Finish the new development
```
git pull origin master
git checkout master
```

Get the stashed stuff
```
git stash pop
```

Any other changes...
```
git add .
git commit -m "Committing stashed stuff in to HEAD"
git push
```

### 3.2. Merge the branch into master
```
git pull
git checkout master
git merge bug_fix_2
```
Resolve conflicts...
Then add the merged result into master.
```
git add .
git commit -m "After Merge"
git push origin master
```

